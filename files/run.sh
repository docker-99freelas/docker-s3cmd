#!/bin/ash
set -e

echo "Running s3cmd ..."

if [ "${SCHEDULE}" = "**None**" ]; then
  s3cmd --config="$CONFIG_PATH"/.s3cfg "$@"
else
  exec go-cron "$SCHEDULE" s3cmd --config="$CONFIG_PATH"/.s3cfg "$@"
fi