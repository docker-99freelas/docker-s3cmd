ARG ALPINE_VERSION=3.8

# Base image to download the sources of s3cmd and go-cron

FROM alpine:${ALPINE_VERSION} AS base

RUN apk update \
 && apk add --no-cache git ca-certificates \
 && mkdir -p /tmp/base/s3cmd \
 && git clone -b 1.0.0 --single-branch --depth 1 \
    https://docker-99freelas@bitbucket.org/docker-99freelas/s3cmd.git /tmp/base/s3cmd

# Final image with s3cmd and go-cron

FROM alpine:${ALPINE_VERSION}

COPY --from=base /tmp/base/s3cmd/ /tmp/base/s3cmd/
COPY files/go-cron-linux.gz /tmp/base/go-cron/
COPY files/run.sh /opt/s3cmd/
COPY files/.s3cfg /opt/s3cmd-conf/

ENV SCHEDULE **None**
ENV CONFIG_PATH /opt/s3cmd-conf

RUN apk update \
 && apk add --no-cache python py-pip py-setuptools ca-certificates \
 && pip install python-magic \
 && cd /tmp/base/s3cmd \
 && python setup.py install \
 && chmod +x /opt/s3cmd/run.sh \
 && zcat /tmp/base/go-cron/go-cron-linux.gz > /usr/local/bin/go-cron \
 && chmod u+x /usr/local/bin/go-cron \
 && rm -rf /tmp/base

WORKDIR /s3

ENTRYPOINT ["/opt/s3cmd/run.sh"]